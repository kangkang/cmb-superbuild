#!/bin/sh

set -e

dnf install -y python3-virtualenv
dnf clean all

readonly venv_root="/opt/venv/girder-client"

mkdir -p "$venv_root"
virtualenv "$venv_root"
"$venv_root/bin/pip" install girder-client

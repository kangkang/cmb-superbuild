# Cubit is a private program to which Kitware developers have access. We provide
# logic for situating these programs into our superbuild for development
# purposes only. Nothing is bundled or distributed.
set(CUBIT_EXE "" CACHE PATH "Location of Cubit executable")

if (NOT EXISTS ${CUBIT_EXE})
  message(FATAL_ERROR "Could not access Cubit executable at ${CUBIT_EXE}")
endif ()

set(opencv_args)
if (numpy_built_by_superbuild)
  if (WIN32)
    set(opencv_numpy_include_dir <INSTALL_DIR>/bin/Lib/site-packages/numpy/core/include)
  else ()
    set(opencv_numpy_include_dir <INSTALL_DIR>/lib/python${superbuild_python_version}/site-packages/numpy/core/include)
  endif ()
  list(APPEND opencv_args
    -DPYTHON2_NUMPY_INCLUDE_DIRS:PATH=${opencv_numpy_include_dir})
endif ()

superbuild_add_project(opencv
#  DEFAULT_ON
  DEPENDS python numpy
  DEPENDS_OPTIONAL python2 python3
  CMAKE_ARGS
    -DBUILD_PERF_TESTS:BOOL=OFF
    -DBUILD_TESTS:BOOL=OFF
    -DBUILD_opencv_flann:BOOL=OFF
    -DBUILD_opencv_hdf:BOOL=OFF
    -DBUILD_opencv_dnn:BOOL=OFF
    -DBUILD_opencv_videoio:BOOL=OFF
    -DWITH_CUDA:BOOL=OFF
    -DWITH_GTK:BOOL=OFF
    -DWITH_FFMPEG:BOOL=OFF
    -DWITH_VTK:BOOL=OFF
    -DBUILD_opencv_highgui=OFF
    -DWITH_QT=OFF
    -DCMAKE_INSTALL_NAME_DIR:PATH=<INSTALL_DIR>/lib
    ${opencv_args})

if (WIN32)
  superbuild_add_extra_cmake_args(
    -DOpenCV_DIR:PATH=<INSTALL_DIR>/share)
endif ()

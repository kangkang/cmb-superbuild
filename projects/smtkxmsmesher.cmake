superbuild_add_project(smtkxmsmesher
  DEBUGGABLE
  DEPENDS xmsmesher
  DEPENDS_OPTIONAL pybind11 python2 python3
  CMAKE_ARGS
    -Wno-dev
    -DBUILD_SHARED_LIBS:BOOL=ON
    -DBUILD_TESTING:BOOL=OFF
    -DENABLE_PYTHON_WRAPPING:BOOL=${pybind11_enabled}
    -DPYTHON_TARGET_VERSION:STRING=${superbuild_python_version}
    )
